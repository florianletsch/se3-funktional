#lang racket

; Fuer Aufgabe 2
(require se3-bib/flaggen-module)

;KOMMENTAR: 35 / 35 Punkte

; Aufgabe 1.1
;KOMMENTAR: 4 Pkt
; Begruendung:
; Unsere Struktir ist eine Liste von Paaren, die ein Zeichen vom Typ 'char' 
; dem entsprechenden Codewort zuorden - welches als Symbol dargestellt
; wird.
; Wir haben diese Struktur gewaehlt, um sie als assoziative Liste 
; verwenden zu koennen.
; Dies ist sinnvoll, da die Zuordnung von Buchstabe zu Codewort eindeutig
; ist und genau unser folgenden Anwendung entspricht.


(define phonetic-alphabet '(
    (#\A   Alpha)
    (#\B   Bravo)
    (#\C   Charlie)
    (#\D   Delta)
    (#\E   Echo)
    (#\F   Foxtrot)
    (#\G   Golf)
    (#\H   Hotel)
    (#\I   India)
    (#\J   Juliet)
    (#\K   Kilo)
    (#\L   Lima)
    (#\M   Mike)
    (#\N   November)
    (#\O   Oscar)
    (#\P   Papa)
    (#\Q   Quebec)
    (#\R   Romeo)
    (#\S   Sierra)
    (#\T   Tango)
    (#\U   Uniform)
    (#\V   Victor)
    (#\W   Whiskey)
    (#\X   X-ray)
    (#\Y   Yankee)
    (#\Z   Zulu)
    (#\0   Nadazero)
    (#\1   Unaone)
    (#\2   Bissotwo)
    (#\3   Terrathree)
    (#\4   Kartefour)
    (#\5   Panafive)
    (#\6   Soxisix)
    (#\7   Setteseven)
    (#\8   Oktoeight)
    (#\9   Novenine)
    (#\,   Decimal)
    (#\.   Stop)
    ))

; Aufgabe 1.2
;KOMMENTAR: 4 Pkt
; Liefert den Buchstabiercode für ein gegebenes Zeichen vom Typ char
; Wenn fuer das Zeichen kein Code gefunden wird, (ungueltiges Zeichen),
; wird das Zeichen unveraendert zurueck gegeben. 
;
; Aufruf:
; > (get-phonetic-code #\A)
; 'Alpha
(define (get-phonetic-code char)
  (let ([code (assoc char phonetic-alphabet)])
        (if code (cadr code) char)))

; Aufgabe 1.3
;KOMMENTAR: 5 Pkt
; Wandelt einen gegebenen Kleinbuchstaben in einen Großbuchstaben um.
; Wenn das gegebene Zeichen kein Kleinbuchstabe ist, wird es unverändert
; zurückgegeben
;
; Aufruf: 
; > (char-to-uppercase #\g)
; #\G
(define (char-to-uppercase char) 
   ( integer->char ; transfer ASCII code back to char
     ((lambda (keycode) 
      (if (and (>= keycode 97) (<= keycode 122)) ; lower case letter between a and z?
          (- keycode 32) ; change to according upper case letter in ASCII table
          keycode) ; else: just return the same letter as given
      ) (char->integer char)) ; get ASCII code
  ))

; Aufgabe 1.4
;KOMMENTAR: 8 Pkt
; Hilfsfunktion. Wandelt eine Liste von Zeichen in eine Liste von
; Code-Wörtern um.
;
; Aufruf:
; > (phonetic-of-list '(#\a #\b #\c))
; '(Alpha Bravo Charlie)
(define (phonetic-of-list lst)
  (if (empty? lst) lst
  (cons 
   (get-phonetic-code (char-to-uppercase (car lst))) 
   (phonetic-of-list (cdr lst))))
 )

; Wandelt eine Zeichenkette in eine Liste von Buchstabier-Code-Wörtern um.
;
; Aufruf:
; > (phonetic-of-string "foo")
; '(Foxtrot Oscar Oscar)
; > (phonetic-of-string "Arthur Dent")
; '(Alpha Romeo Tango Hotel Uniform Romeo #\space Delta Echo November Tango)
(define (phonetic-of-string str)
  (phonetic-of-list (string->list str)))

; Aufgabe 2.1
;KOMMENTAR: 3 Pkt
; Für diese Datenstruktur wählen wir eine kurze statische Liste, um die Zahlen auf 
; das Symbol abzubilden, das das Flaggensymbol präsentiert. Dies funktioniert,
; da das importierte Modul die Flaggen in Rackets Symboltabelle abgelegt hat.
; Die Buchstaben werden nicht statisch in der Liste abgelegt, da ihr Symbol 
; automatisch berechnet werden kann. (siehe Aufgabe 2.2)

; Wandelt ein einzelnes Zeichen (nur Zeichen für Zahlen erlaubt) in ein Flaggensymbol um
; Aufruf:
; > (flags-alphabet #\1)
; ... ; gibt ein grafisches Flaggensymbol aus

(define flags-alphabet '(
  (#\0   Z0)
  (#\1   Z1)
  (#\2   Z2)
  (#\3   Z3)
  (#\4   Z4)
  (#\5   Z5)
  (#\6   Z6)
  (#\7   Z7)
  (#\8   Z8)
  (#\9   Z9))) ; letters are being mapped automatically

; Aufgabe 2.2
;KOMMENTAR: 3 Pkt
; Wandelt ein Zeichen vom Typ char in ein Flaggensymbol um.
;
; Aufruf:
; > (char-to-flag #\A)
; ... Flaggensymbol
(define (char-to-flag char)
    (let ([flag (assoc char flags-alphabet)] [keycode (char->integer char)])
        (if flag
            (eval (cadr flag)) ; exists in flag table, so it's a number  
            (if (and (>= keycode 65) (<= keycode  90)) ; uppercase letter?
              (eval (string->symbol (make-string 1 char))) ; convert capital letter
              char) ; no number, no letter: return character unchanged 
    )))

; Aufgabe 2.3
;KOMMENTAR: 8 Pkt

; Hilfsfunktion
; Wandelt eine Liste von Buchstaben oder Zahlen in eine Liste von Flaggen um.
; 
; Aufruf:
; > (flags-of-list '(#\A #\b #\1))
; (list ... ... ...) ; 3 Flaggensymbole
(define (flags-of-list lst)
  (if (empty? lst) lst
  (cons 
   (char-to-flag (char-to-uppercase (car lst))) 
   (flags-of-list (cdr lst))))
 )


; Wandelt eine Zeichenkette in eine Liste von Flaggen um, wobei jede Flagge 
; dem eingegebenen Zeichen entspricht (nach Flaggentabelle der Seefahrt)
; Die Zeichenkette darf beliebige Zeichen enthalten, Flaggen werden nur erzeugt
; für Kleinbuchstaben, Großbuchstaben und Zahlen.
;
; Aufruf:
; > (flags-of-string "foo")
; (list .....) ; 3 Flaggensymbole
(define (flags-of-string str)
  (flags-of-list (string->list str)))
