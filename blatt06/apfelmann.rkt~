#lang racket/gui

(require racket/future)
(require future-visualizer)
(require 2htdp/image)

; returns color corresponding to time needed to calculate if the complex number is a member of the mandelbrot set
; complex: complex number to test
; iterations: maximum iterations for testing (proportional to accuracy; anti-proportional to performance)
; alpha: Alpha value for the numbers which are NOT in the mandelbrot set
(define (mandelbrot-color complex iterations alpha)
  (mandelbrot-color-helper complex 0 iterations iterations alpha)
  )

; helper class fot mandelbrot-color (wrapped because of the large number of arguments needed)
(define (mandelbrot-color-helper complex x iterations total-iterations alpha)
  ; if all iterations have been completed return 'black' (means, the number is [probably] in the mandelbrot set)
  (cond[(= 0 iterations)
        (make-color 0 0 0)]
       ; else apply the mandelbrot formular on the iteration variable
       [else (let ([x (+ (* x x) complex)])
               ; if the magnitude of the iteration variable (i.e. it's abs value) is greater than 2, the complex number is not in the mandelbrot set
               (cond [(> (magnitude x) 2)
                      ; in this case calculate a fancy color value based on the iterations needed to get the result (I use different prime numbers as modulo to ensure a good variety of colors)
                      (make-color (* 1 (modulo (- total-iterations iterations) 251)) ; red - value between 0 and 240
                                  (* 2 (modulo (- total-iterations iterations) 113)) ; green - value between 0 and 192
                                  (* 3 (modulo (- total-iterations iterations) 83)) ; blue - value between 0 and 70
                                  alpha)]
                     ; else test the next step of recursion
                     [else (mandelbrot-color-helper complex x (- iterations 1) total-iterations alpha)]))])
  )

; returns the specified selection of the mandelbrot set in a list representations (1 entry in the list is one pixel)
; arguments are explained inside the function
(define (mandelbrot-selection width height offset-left offset-right offset-top offset-bottom iterations alpha parallel?)
  (cond [parallel?
         (mandelbrot-set-parallel (- width offset-right) ; this will be the start value to count down for the width (and the right border of the view)
                                  (- height offset-bottom) ; this will be the start value to count down for the height (and the bottom border of the view)
                                  width ; this is the width of the overall plane containing the mandelbrot set
                                  height ; this is the height of the overall plane
                                  offset-left ; this will be the end value to count down for the width (and the left border of the view)
                                  offset-top ; this will be the end value to count down for the width (and the top border of the view)
                                  iterations ; the number of iterations to calculate (see function 'mandelbrot-color')
                                  alpha)] ; the transparency of the colors (see function 'mandelbrot-color')
        [else
         (mandelbrot-set (- width offset-right) ; this will be the start value to count down for the width (and the right border of the view)
                                  (- height offset-bottom) ; this will be the start value to count down for the height (and the bottom border of the view)
                                  width ; this is the width of the overall plane containing the mandelbrot set
                                  height ; this is the height of the overall plane
                                  offset-left ; this will be the end value to count down for the width (and the left border of the view)
                                  offset-top ; this will be the end value to count down for the width (and the top border of the view)
                                  iterations ; the number of iterations to calculate (see function 'mandelbrot-color')
                                  alpha)]) ; the transparency of the colors (see function 'mandelbrot-color')
  )

; returns a list of colors representing the mandelbrot set
; col: the number of columns to display
; row: the number of rows to display
; width: the width of the whole mandelbrot set (effectively defining the resolution)
; height: the height of the whole mandelbrot set (effectively defining the resolution)
; offset-left: the left border of the clipping
; offset-top: the top border of the clipping
; iterations: the number of iterations to calculate (see function 'mandelbrot-color')
; alpha: the transparency of the colors (see function 'mandelbrot-color')
(define (mandelbrot-set col row width height offset-left offset-top iterations alpha)
  ; return an empty list if row is equal to the left border of the section to display
  (cond [(= offset-top row)
         '()]
        ; else append a new row to the list, decrease the row index by one and call itself recursively
        [else
         (append 
          (mandelbrot-set col (- row 1) width height offset-left offset-top iterations alpha) 
          (fill-row col row width height offset-left iterations alpha))])
  )

; returns a list of colors representing the mandelbrot set
; col: the number of columns to display
; row: the number of rows to display
; width: the width of the whole mandelbrot set (effectively defining the resolution)
; height: the height of the whole mandelbrot set (effectively defining the resolution)
; offset-left: the left border of the clipping
; offset-top: the top border of the clipping
; iterations: the number of iterations to calculate (see function 'mandelbrot-color')
; alpha: the transparency of the colors (see function 'mandelbrot-color')
(define (mandelbrot-set-parallel col row width height offset-left offset-top iterations alpha)
  ; append all elements of the list (since a 2-dimensional list is returned by map)
  (apply append 
         (map 
          ; if the element of the list is a future, return its result (as soon as available)
          (lambda (f) (cond [(future? f)
                             (touch f)])) 
          ; retrieves a list of futures (single processes); one for each row being calculated
          (get-futures col row width height offset-left offset-top iterations alpha)))
  )

; returns a list of futures each holding one process calculating a row of the mandelbrot set
; col: the number of columns to display
; row: the number of rows to display
; width: the width of the whole mandelbrot set (effectively defining the resolution)
; height: the height of the whole mandelbrot set (effectively defining the resolution)
; offset-left: the left border of the clipping
; offset-top: the top border of the clipping
; iterations: the number of iterations to calculate (see function 'mandelbrot-color')
; alpha: the transparency of the colors (see function 'mandelbrot-color')
(define (get-futures col row width height offset-left offset-top iterations alpha)
  ; return an empty list if row is equal to the left border of the section to display
  (cond [(= offset-top row) '()]
        ; else append a future (cast to a list) holding a process which calculates one row of the mandelbrot set
        ; to the end of the result of it's recursive invocation
        [else (append 
               (get-futures col (- row 1) width height offset-left offset-top iterations alpha) 
               (list (future (lambda () (fill-row col row width height offset-left iterations alpha)))))])
  )

; returns a list of colors which are calculated based on the coordinates in the complex plane
; col: the number of columns to display
; row: the current row (needed for calculation of the complex number)
; width: the width of the whole mandelbrot set (effectively defining the resolution)
; height: the height of the whole mandelbrot set (effectively defining the resolution)
; offset-left: the left border of the clipping
; iterations: the number of iterations to calculate (see function 'mandelbrot-color')
; alpha: the transparency of the colors (see function 'mandelbrot-color')
(define (fill-row col row width height offset-left iterations alpha)
  ; return an empty list if col is equal to the left border of the section to display
  (cond [(= offset-left col)
         '()]
        ; recieve the color for the current number from (mandelbrot-color), decrease the column index by one and append the color to the list returned by the recursive self invocation.
        [else
         (append (fill-row (- col 1) row width height offset-left iterations alpha) 
                 ; (list ...) is required because append would throw an error otherwise. If you have a better solution, please correct it. :)
                 (list (mandelbrot-color
                        (make-rectangular (- (* 3 (/ col width)) 2.25 )      ; calculating the complex number from real part
                                          (- (* 2.25 (/ row height)) 1.25))  ; and imaginary part
                        iterations 
                        alpha)))])
  )

; draws a representation of the mandelbrot set in the complex plane.
; width: the width of the whole mandelbrot set (effectively defining it's resolution)
; height: the height of the whole mandelbrot set (effectively defining the resolution)
; offset-left: the left border of the clipping
; offset-right: the right border of the clipping
; offset-top: the top border of the clipping
; offset-bottom: the bottom border of the clipping
; scale: a factor applied to all dimensional arguments (width, height and offsets)
; iterations: the number of iterations to calculate (see function 'mandelbrot-color')
; alpha: the transparency of the colors (see function 'mandelbrot-color')
(define (draw-mandelbrot width height offset-left offset-right offset-top offset-bottom scale iterations alpha)
  (display "Calculating Mandelbrot set... This may take some time.\nIf this process takes too long, please decrease the scale of the selection or the iterations factor.\n")
  (color-list->bitmap 
   (mandelbrot-selection 
    (* scale width)
    (* scale height)
    (* scale offset-left)
    (* scale offset-right)
    (* scale offset-top)
    (* scale offset-bottom)
    iterations 
    alpha
    #f) ; parallelisation is turned off since it seems to crash for big parallel processes (assumingly in terms of RAM)
   (* scale (- width  offset-left offset-right)) 
   (* scale (- height offset-top offset-bottom))
   )
  )


;(future (lambda () (fill-row 5 5 20 20 0 100 255)))
;(mandelbrot-set-parallel 5 5 20 20 0 0 100 255)

; Example, featuring a nice section of the mandelbrot set (Attention! Takes some time...) :D
(let* ([scale 16]
       [width 900]
       [height 600]
       [offset-left 655]
       [offset-right 215]
       [offset-top 145]
       [offset-bottom 433]
       [iterations 100]
       [alpha 155])
  ; (visualize-futures ; used fo debugging with parallelization
  (place-image
   (draw-mandelbrot width height offset-left offset-right offset-top offset-bottom scale iterations alpha)
   0 0 
   (empty-scene (* (- width offset-left offset-right) scale) 
                (* (- height offset-top offset-bottom) scale)
                "darkred")
  )
  ; )
  )

