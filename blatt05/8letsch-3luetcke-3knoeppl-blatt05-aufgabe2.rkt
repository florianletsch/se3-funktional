#lang racket

; For visual output of butterflies
(require se3-bib/butterfly-module-2014)

; Aufgabe 2: FARBMUTATIONEN (CODE KOPIERT VON AUFGABE 1)

; VERÄNDERT FÜR AUFGABE 2
; List of possible traits for all phenotype characters, ordered from dominant to recessive 
(define traits 
  (list
    '(purple blue green yellow red orange) ; color
    '(stripes dots stars) ; pattern
    '(curved straight curly) ; feeler shape
    '(hexagon rhomb ellipse) ; wing shape
))

; Helper function
;
; Returns a random list element 
; 
; Call:
; > (random-element '(1 2 3 4 5))
; 4
(define (random-element list)
  (list-ref list (random (length list))))


; Generates a child from two given parents. The traits for all phenotype characters are
; assigned randomly, but according to Mendel's rules
;
; parent1: Genotype of first parent
; parent2: Genotype of second parent
; Returns: Genotype of child
;
; Call:
; > (generate-child '((blue red) (stripes stars) (curved straight) (hexagon ellipse)) '((yellow yellow) (dots stars) (straight curly) (rhomb ellipse)))
; '((blue yellow) (stars dots) (curved straight) (hexagon ellipse))
(define (generate-child parent1 parent2)
  ( list
    (list (apply color-mutation (car parent1)) (apply color-mutation (car parent2)) ) ; color
    (list (apply color-mutation (second parent1)) (apply color-mutation (second parent2)) ) ; pattern
    (list (apply color-mutation (third parent1)) (apply color-mutation (third parent2)) ) ; feeler shape   
    (list (apply color-mutation (fourth parent1)) (apply color-mutation (fourth parent2)) ) ; wing shape  
))

; Generates a given number of butterflies based on 2 parents.
;
; parent1: The genotype of the first parent, given as a list of lists
; parent2: The genotype of the first parent, given as a list of lists
; count:   How many children to generate, given as integer
;
; Call:
; > (generate-children father mother 2)
; '(((yellow red) (stars stars) (straight curved) (rhomb ellipse))
;  ((yellow red) (stars stripes) (straight curved) (ellipse ellipse)))
;
(define (generate-children parent1 parent2 count)
  (if (<= count 0) '()
      (cons (generate-child parent1 parent2) (generate-children parent1 parent2 (- count 1)))
  )
)

; Returns the dominant one of two given traits.
;
; traits: List of 2 traits, out of which to return the dominant one
; domincance: List of traits, ordered from dominant to recessive
;
; Call:
; > (find-dominant '(blue red) '(blue yellow red))
; 'blue
(define (find-dominant traits dominance)
  (let ([trait1 (first traits)] [trait2 (second traits)])
    (if (empty? dominance) #f
      (if (eq? trait1 (car dominance)) trait1
        (if (eq? trait2 (car dominance)) trait2
          (find-dominant traits (cdr dominance))
        )
      ) ; if
    ); if
  ); let
)

; Returns the phenotype of a butterfly from a given genotype
;
; genotype: The butterfly's genotype, given as a list of all characteristics
;
; Call: 
; > (phenotype '((blue yellow) (stars dots) (curved straight) (hexagon ellipse)))
; '(blue dots curved hexagon)
(define (phenotype genotype) 
  (list 
   (find-dominant (first genotype) (first traits))
   (find-dominant (second genotype) (second traits))
   (find-dominant (third genotype) (third traits))
   (find-dominant (fourth genotype) (fourth traits))
   )
  )


; Generates and displays a given number of butterflies
; who are descendants of a given pair of parents.
;
; parent1: The genotype of the first parent, given as a list of lists
; parent2: The genotype of the first parent, given as a list of lists
; count:   How many children to generate and display, given as integer
;
; Call:
; > (display-children mother father 5)
; (list ... ... ...) ; display images of the two parents and 5 children
(define (display-children parent1 parent2 count)
  (map (lambda (butterfly) (apply show-butterfly (phenotype butterfly)))
       (append (list parent1 parent2) (generate-children parent1 parent2 count))
  ) ; map
) ; define

; Aufgabe 2: FARBMUTATIONEN

(define (color-mutation color1 color2) 
  (let* (
    [mutation-type (match (list color1 color2) ; determine mutation type (which colors are occuring?)
      [(list 'blue 'yellow) 0] 
      [(list 'yellow 'blue) 0]
      [(list 'blue 'red) 1]
      [(list 'red 'blue) 1]
      [(list 'red 'yellow) 2]
      [(list 'yellow 'red) 2]
      [(list _ _) 3])] ; case: no mutation
    [mutation (list-ref '(green purple orange #f) mutation-type)]
    [probability (list-ref '(0.3 0.15 0.75 0) mutation-type)])
    (case mutation-type 
      [(0 1 2) (if (<= (random) probability) 
                   mutation ; mutation has happened!
                   (random-element (list color1 color2)))] 
      [(3) (random-element (list color1 color2))])
    )
)

; EXAMPLE:
; two butterflies defined by their genotype
(define mother '( (blue red) (stripes stars) (curved straight) (hexagon ellipse)))
(define father '( (yellow yellow) (dots stars) (straight curly) (rhomb ellipse)))

; > (generate-children mother father 5)
; '(((purple yellow) (stripes stars) (straight straight) (hexagon rhomb))
;  ((blue yellow) (stripes dots) (curved curly) (ellipse rhomb))
;  ((red yellow) (stripes stars) (curved straight) (ellipse ellipse))
;  ((red yellow) (stripes stars) (curved straight) (hexagon rhomb))
;  ((blue yellow) (stripes dots) (straight straight) (hexagon rhomb)))

; Look at this, it's pretty!:
; > (display-children mother father 30)

; Phenotype is generated according to dominance of traits
; > (phenotype '((lilac yellow) (stars stars) (curved curly) (ellipse rhomb)))
; '(lilac stars curved rhomb)