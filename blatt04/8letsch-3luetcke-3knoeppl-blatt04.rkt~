#lang racket

(require racket/date)

;; Aufgabe 1
;;
;; 1. -3
;; 2. 0
;; 3. 'Jahre
;; 4. '()
;; 5. 'Weihnachtsfest
;; 6. '(Listen ganz einfach und)
;; 7. '(Paare . auch)
;; 8. #t
;; 9. #f


;; Aufgabe 2
;;
;; 1.

;; Grammatik für Notmeldungen in Backus-Naur Form

;; <notmeldung> ::= <ueberschrift><standortangabe><weitereAngaben><peilzeichen><unterschrift><over>

;; <ueberschrift> ::= <notzeichen3x> HIER IST <schiffsname3x> <rufzeichen> <notzeichen> <schiffsnameBuchstabiert> <rufzeichenBuchstabiert>
;; <standortangabe> ::= NOTFALLPOSITION [<text>] <position> NOTFALLZEIT [<text>] <zeitangabe> [<text>]
;; <weitereAngaben> ::= <artdesnotfalls><angabenzurhilfeleistung>
;; <artdesnotfalls> ::= <text>
;; <angabenzurhilfeleistung> ::= <text>
;; <peilzeichen> ::= --
;; <unterschrift> ::= <schiffsname> <rufzeichen-buchstabiert>
;; <over> ::= OVER

;; <notzeichen3x> ::= <notzeichen><notzeichen><notzeichen>
;; <hierIstText> ::= HIER IST
;; <schiffsname3x> ::= <schiffsname><schiffsname><schiffsname>
;; <rufzeichen> ::= RUFZEICHEN <word><word><word><word>
;; <notzeichen> ::= MAYDAY
;; <schiffsnameBuchstabiert> ::= <word>|<word><schiffsnameBuchstabiert>
;; <rufzeichenBuchstabiert>

;; <text> ::= <literal>|<literal><text>
;; <position> ::= <wert> <streckenEinheit> <richtung> <ort>
;; <zeitangabe> ::= <wert> <zeitZone>
;; <schiffsname> ::= <text>
;; <rufzeichenBuchstabiert> ::= <word>|<word><rufzeichenBuchstabiert>

;; <wert> ::= <number>|<number><wert>
;; <streckenEinheit> ::= SM|Seemeilen|miles
;; <zeitZone> ::= UTC
;; <word> ::=Alpha|Bravo|Charlie|Delta|Echo|Foxtrot|Golf|Hotel|India|Juliet|Kilo|Lima|Mike|November|Oscar|Papa|Quebec|Romeo|Sierra|Tango|Uniform|Victor|Whiskey|X-ray|Yankee|Zulu|Nadazero|Unaone|Bissotwo|Terrathree|Kartefour|Panafive|Soxisix|Setteseven|Oktoeight|Novenine|Decimal|Stop

;; <number> ::= "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
;; <literal> ::= "A"|"B"|"C"|"D"|"E"|"F"|"G"|"H"|"I"|"J"|"K"|"L"|"M"|"N"|"O"|"P"|"Q"|"R"|"S"|"T"|"U"|"V"|"W"|"X"|"Y"|"Z"

;; 2. Generator
;
; Generiert einen wohlgeformten Notruf.
;
; Aufruf:
; (notmeldung
;          "BABETTE"
;          "DEJY"
;          "10 SM NORDOESTLICH LEUCHTTURM KIEL"
;          "SCHWERER WASSEREINBRUCH WIR SINKEN ...")
(define (notmeldung schiffsname-value rufzeichen-value position-value weitereangaben-value)
  (string-append (ueberschrift schiffsname-value rufzeichen-value) "\n"
                 (standortangabe position-value "1000 UTC") "\n" ;;TODO: (if over-motivated) wanted to create the date dynamically from current time but did not fully understand the 'date' struct.
                 weitereangaben-value " --\n"
                 (unterschrift schiffsname-value rufzeichen-value) "\n"
                 "OVER")
  )

; Generiert eine wohlgeformte Überschrift für einen Notruf
;
; Aufruf:
; (ueberschrift "Babette" "DEJY")
(define (ueberschrift schiffsname-value rufzeichen-value)
  (string-append
   (repeat "MAYDAY" 3)"\n"
            "HIER IST\n" (repeat schiffsname-value 3) " " (in-words rufzeichen-value) "\n"
            "MAYDAY " schiffsname-value " ICH BUCHSTABIERE " (in-words schiffsname-value) "\n"
            "RUFZEICHEN " (in-words rufzeichen-value))
  )

; Generiert eine wohlgeformte Standortangabe
;
; Aufruf:
; (standortangabe "10 SM NORDOESTLICH LEUCHTTURM KIEL" "1000 UTC")
(define (standortangabe position-value zeitangabe-value)
  (string-append "NOTFALLPOSITION " position-value
                 "\nNOTFALLZEIT " zeitangabe-value)
  )

(define (zeitangabe)
  (let* ((now (current-date)) ; FIXME: das muss noch UTC sein!
        (hours (number->string (date-hour now)))
        (minutes (number->string (date-minute now)))
        (timezone "CET")) ; TODO: dynamisch vom datum auslesen
        (string-append hours minutes " " timezone)
  )
  )

; Generiert eine wohlgeformte Unterschrift für einen Notruf
;
; Aufruf:
; (unterschrift "Babette" "DEJY")
(define (unterschrift schiffsname-value rufzeichen-value)
  (string-append schiffsname-value " " (in-words rufzeichen-value))
  )

;; Helper functions

; Wiederholt einen String mehrmals mit einem Leerzeichen als Trenner
;
; Aufruf:
; > (repeat "kakapo" 3)
; "kakapo kakapo kakapo"
(define (repeat string times)
  (cond [(= 1 times) string]
        [else
         (string-append string " " (repeat string (- times 1)))])
  )

; Gibt die Rufzeichen für ein gegebenes Wort in Großbuchstaben zurück
;
; Aufruf:
; > (in-words "FEYNMAN")
; "Foxtrot Echo Yankee November Mike Alpha November "
(define (in-words string)
  (cond [(= 0 (string-length string))
         ""]
        [else
         (string-append (get-word (first (string->list string)))
                        " "
                        (in-words (substring string 1)))])
  )

; Gibt das Codewort für einen gegebenes Zeichen zurück
;
; Aufruf:
; > (get-word #\A)
; "Alpha"
(define (get-word char)
  (symbol->string (cadr (assoc char phonetic-alphabet)))
  )

(define phonetic-alphabet '(
    (#\A   Alpha)
    (#\B   Bravo)
    (#\C   Charlie)
    (#\D   Delta)
    (#\E   Echo)
    (#\F   Foxtrot)
    (#\G   Golf)
    (#\H   Hotel)
    (#\I   India)
    (#\J   Juliet)
    (#\K   Kilo)
    (#\L   Lima)
    (#\M   Mike)
    (#\N   November)
    (#\O   Oscar)
    (#\P   Papa)
    (#\Q   Quebec)
    (#\R   Romeo)
    (#\S   Sierra)
    (#\T   Tango)
    (#\U   Uniform)
    (#\V   Victor)
    (#\W   Whiskey)
    (#\X   X-ray)
    (#\Y   Yankee)
    (#\Z   Zulu)
    (#\0   Nadazero)
    (#\1   Unaone)
    (#\2   Bissotwo)
    (#\3   Terrathree)
    (#\4   Kartefour)
    (#\5   Panafive)
    (#\6   Soxisix)
    (#\7   Setteseven)
    (#\8   Oktoeight)
    (#\9   Novenine)
    (#\,   Decimal)
    (#\.   Stop)
    ))

(define d (date 1    ;seconds
                2    ;minutes
                3    ;hour
                20   ;day
                8    ;month
                2012 ;year
                0    ;weekday  <<<
                0    ;year-day <<<
                #f   ;dst?
                0    ;time-zone-offset
                ))

;; 3. a) Der Test (BABETTE):

(display (notmeldung
          "BABETTE"
          "DEJY"
          "10 SM NORDOESTLICH LEUCHTTURM KIEL"
          (string-append "SCHWERER WASSEREINBRUCH WIR SINKEN\n"
                         "KEINE VERLETZTEN\n"
                         "VIER MANN GEHEN IN DIE RETTUNGSINSEL\n"
                         "SCHNELLE HILFE ERFORDERLICH\n"
                         "ICH SENDE DEN TRAEGER")))

(display "\n\n\n")

;; 3. b) Der Test (AMIRA):

(display (notmeldung
          "AMIRA"
          "AMRY"
          "53°56'N, 006°31'E"
          (string-append "SCHWERER WASSEREINBRUCH WIR SINKEN\n"
                         "KEINE VERLETZTEN\n"
                         "VIER MANN GEHEN IN DIE RETTUNGSINSEL\n"
                         "SCHNELLE HILFE ERFORDERLICH\n"
                         "ICH SENDE DEN TRAEGER")))


; Aufgabe 3: FUNKTIONEN VS SPEZIALFORMEN

;;;; Aufgabe 3.1
; Innere Reduktion: Ausdrücke werden von innen nach außen ausgewertet. Das heißt, dass
; jeder Teilausdruck ausgewertet wird, bevor der Gesamtausdruck auf Grundlage der
; Teilausdrücke ausgewertet wird.
; Äußere Reduktion: Ausdrücke werden von außen nach innen ausgewertet. Das heißt,
; das Ausdrücke gemäß ihrer Form ausgewertet werden und Teilausdrucke erst dann
; ausgewertet werden, wenn sie für die Auswertung des Gesamtausdrucks benötigt werden.
;
; Der größte Unterschied besteht also darin, in welcher Reihenfolge die Auswertung stattfindet.
; Bei äußerer Auswertung müssen zudem Teilausdrücke nicht ausgewertet werden, wenn sie nicht
; benötigt werden. Ein gutes Beispiel hierfür ist das "if", bei dem die Teilausdrücke
; in Abhängigkeit der Bedingung ausgewertet werden oder eben nicht.
; Dafür werden bei äußerer Reduktion Parameter, die in einer Funktion mehrfach vorkommen,
; auch mehrfach ausgewerte, was zu höherem Berechnungsaufwand führen kann.
;
; Beispiel:
; (define (hoch3 x) (* x x x))
; (hoch3 (* 3 (+ 1 (hoch3 2))))
;
; 1. Innere Auswertung (wie in Racket)
;    (hoch3 (* 3 (+ 1 (hoch3 2))))
; -> (hoch3 (* 3 (+ 1 (* 2 2 2))))
; -> (hoch3 (* 3 (+ 1 8)))
; -> (hoch3 (* 3 9))
; -> (hoch3 27)
; -> (* 27 27 27)
; -> 19683
;
; 2. Äußere Auswertung (wie in Haskell oder Miranda)
;    (hoch3 (* 3 (+ 1 (hoch3 2))))
; -> (* (* 3 (+ 1 (hoch3 2))) (* 3 (+ 1 (hoch3 2))) (* 3 (+ 1 (hoch3 2))))
; -> (* (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (hoch3 2))) (* 3 (+ 1 (hoch3 2))))
; -> (* (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (hoch3 2))))
; -> (* (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (* 2 2 2))))
; -> (* (* 3 (+ 1 8)) (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (* 2 2 2))))
; -> (* (* 3 9) (* 3 (+ 1 (* 2 2 2))) (* 3 (+ 1 (* 2 2 2))))
; -> (* 27 (* 3 (+ 1 8)) (* 3 (+ 1 (* 2 2 2))))
; -> (* 27 (* 3 9) (* 3 (+ 1 (* 2 2 2))))
; -> (* 27 27 (* 3 (+ 1 (* 2 2 2))))
; -> (* 27 27 (* 3 (+ 1 8)))
; -> (* 27 27 (* 3 9))
; -> (* 27 27 27)
; -> 19683
;
;;;; Aufgabe 3.2
; Racket verwendet innere Auswertung für Funktionen (siehe obiges Beispiel).
; In einigen Fällen ist äußere Auswertung aber notwendig (bspw. für "if").
; Daher gibt es special form expressions, die in Racket mit äußerer Auswertung
; ausgewertet werden.
;
;;;; Aufgabe 3.3
; Wenn Alyssa dieses Programm benutzt, wird es nicht terminieren (bzw. wird die Laufzeitumgebung in
; Racket das Programm ab einer bestimmten Speichernutzung abbrechen).
; Dies liegt daran, dass das "new-if" keine special form expression ist (wie Rackets eigenes "if").
; Daher wird new-if als Funktion mit innerer Auswertung ausgewertet. Daher wird der rekursive Aufruf
; von faculty in jedem Fall ausgewertet, unabhängig von der Abbruchbedingung. Dies geschieht in jedem
; Rekursionsschritt, so dass die Rekursion nicht zum Abschluss kommt.
;
;(define (new-if condition? then-clause else-clause)
;  (cond ( condition? then-clause )
;  (else else-clause )))

;(define (faculty product counter max-count)
;  (new-if (> counter max-count)
;    product
;    (faculty (* counter product)
;    (+ counter 1) max-count )
;  )
;)

